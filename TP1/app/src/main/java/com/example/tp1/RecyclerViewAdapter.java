package com.example.tp1;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "RecyclerViewAdapter";
    private String[] nameList;
    private Context aContext;

    public RecyclerViewAdapter(Context context){
        aContext = context;
        nameList = AnimalList.get().getNameArray();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.animauxliste, parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.name.setText(nameList[position]);
        holder.ani_image.setImageResource( AnimalList.get().getAnimal(nameList[position]).getImgFile());

        holder.pLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(aContext, DescriActivity.class);
                intent.putExtra("aname",nameList[position]);

                Animal animal =  AnimalList.get().getAnimal(nameList[position]);
                intent.putExtra("vie", animal.getStrHightestLifespan());
                intent.putExtra("img", animal.getImgFile());
                intent.putExtra("gestation", animal.getStrGestationPeriod());
                intent.putExtra("poidsn", animal.getStrBirthWeight());
                intent.putExtra("poidsa", animal.getStrAdultWeight());
                intent.putExtra("statut", animal.getConservationStatus());
                aContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return nameList.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView name;
        RelativeLayout pLayout;
        ImageView ani_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.animal_name);
            pLayout = itemView.findViewById(R.id.pLayout);
            ani_image = itemView.findViewById(R.id.animal_image);
        }
    }
}
