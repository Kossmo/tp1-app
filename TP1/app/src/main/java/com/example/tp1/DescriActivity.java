package com.example.tp1;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class DescriActivity extends AppCompatActivity {

    private static final String TAG = "DescriActivity";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animauxdescri);

        getIncomingIntent();
    }

    private void getIncomingIntent() {
        if (getIntent().hasExtra("aname") && getIntent().hasExtra("vie") && getIntent().hasExtra("img") && getIntent().hasExtra("gestation") && getIntent().hasExtra("poidsn") && getIntent().hasExtra("poidsa") && getIntent().hasExtra("statut")) {
            String aname = getIntent().getStringExtra("aname");
            String vie = getIntent().getStringExtra("vie");
            int img = getIntent().getIntExtra("img",0);
            String gestation = getIntent().getStringExtra("gestation");
            String poidsn = getIntent().getStringExtra("poidsn");
            String poidsa = getIntent().getStringExtra("poidsa");
            String statut = getIntent().getStringExtra("statut");
            setValue(aname, vie, img, gestation, poidsn, poidsa, statut);
        }
    }

    private void setValue(String aname, String vie, int img, String gestation, String poidsn, String poidsa, String statut){
        final TextView descri_nom = findViewById(R.id.descri_nom);
        descri_nom.setText(aname);
        ImageView descri_img = findViewById(R.id.descri_img);
        descri_img.setImageResource(img);
        final TextView descri_edit1 = findViewById(R.id.descri_edit1);
        descri_edit1.setText(String.valueOf(vie));
        final TextView descri_edit2 = findViewById(R.id.descri_edit2);
        descri_edit2.setText(String.valueOf(gestation));
        final TextView descri_edit3 = findViewById(R.id.descri_edit3);
        descri_edit3.setText(String.valueOf(poidsn));
        final TextView descri_edit4 = findViewById(R.id.descri_edit4);
        descri_edit4.setText(String.valueOf(poidsa));
        final EditText descri_edit5 = findViewById(R.id.descri_edit5);
        descri_edit5.setText(statut);

        Button button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                @SuppressLint("ResourceType") String nom = String.valueOf(descri_nom.getText());
                @SuppressLint("ResourceType") String statut_modif = String.valueOf(descri_edit5.getText());

                Animal animal_modif = AnimalList.get().getAnimal(nom);
                animal_modif.setConservationStatus(statut_modif);
            }
        });
    }
}
