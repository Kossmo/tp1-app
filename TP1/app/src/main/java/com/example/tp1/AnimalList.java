package com.example.tp1;

import java.util.HashMap;

public class AnimalList {

    private static AnimalList singleton;
    private static HashMap<String, Animal> hashMap = init();

    private static HashMap<String, Animal> init() {
        HashMap<String, Animal> res = new HashMap<>();
        res.put("Ours brun", new Animal(40, R.drawable.bear, 210, 0.5f, 278, "préoccupation mineure"));
        res.put("Chameau", new Animal(36, R.drawable.camel, 395, 36f, 475, "préoccupation mineure"));
        res.put("Montbéliarde", new Animal(8, R.drawable.cow, 288, 190f, 900, "préoccupation mineure"));
        res.put("Renard roux", new Animal(21, R.drawable.fox, 52, 0.1f, 4, "préoccupation mineure"));
        res.put("Koala", new Animal(22, R.drawable.koala, 35, 0.4f, 9, "vulnérable"));
        res.put("Lion", new Animal(27, R.drawable.lion, 108, 1.3f, 180, "vulnérable"));
        res.put("Panda géant", new Animal(37, R.drawable.panda, 130, 0.1f, 118, "vulnérable"));
        return res;
    }

    public static String[] getNameArray() {
        return hashMap.keySet().toArray(new String[hashMap.size()]);
    }

    public static Animal getAnimal(String name) {
        return hashMap.get(name);
    }

    public static  AnimalList get(){
        if(singleton == null) {
            singleton = new AnimalList();
        }
        return singleton;
    }
}
